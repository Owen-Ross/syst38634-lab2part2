package password;

/*
 * @author Owen Ross, 991484122
 *  
 * This class validates passwords and it will be developed using TDD
 */


public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_NUM_DIGITS = 2;
	
	public static boolean isValidLength(String password) {
		if (password != null) {
		return password.trim().length() >= MIN_LENGTH;
		}
		return false;
	}
	
	public static boolean hasEnoughDigits(String password) {
		int totalDigits = 0;
		
		if(password != null) {
		for(int i = 0; i < password.length(); i++) {
			if(Character.isDigit(password.charAt(i))) {
				totalDigits++;
			}
		}
		
		return totalDigits >= MIN_NUM_DIGITS;
		}
		
		return false;
		
	}
}
