package password;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * @author Owen Ross, 991484122
 * 
 * This class will be used to perform unit tests on the PasswordValidator class 
 * and it will be developed using TDD
 * 
 * Assumption is spaces are not considered valid characters
 */

public class PasswordValidatorTest {

	@Test
	public void testIsValidLength() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("1234567890"));
	}

	@Test
	public void testIsValidLengthException() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength(null));
	}
	
	@Test
	public void testIsValidLengthExceptionSpaces() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("           "));
	}
	
	@Test
	public void testIsValidLengthBoundryIn() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("12345678"));
	}
	
	@Test
	public void testIsValidLengthBoundryOut() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("1234567"));
	}
	
	@Test
	public void testhasEnoughDigits() {
		assertTrue("Invalid number of digits", PasswordValidator.hasEnoughDigits("qwerty4897"));
	}
	
	@Test
	public void testhasEnoughDigitsException() {
		assertFalse("Invalid number of digits", PasswordValidator.hasEnoughDigits(null));
	}
	
	@Test
	public void testhasEnoughDigitsBoundraryIn() {
		assertTrue("Invalid number of digits", PasswordValidator.hasEnoughDigits("fdgrsth26"));
	}
	
	@Test
	public void testhasEnoughDigitsBoundraryOut() {
		assertFalse("Invalid number of digits", PasswordValidator.hasEnoughDigits("hgfdt6jbs"));
	}
}
